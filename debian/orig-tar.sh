#!/bin/sh -e
set -x
VERSION=$2
TAR=../libopensaml2-java_$VERSION.orig.tar.xz
DIR=libopensaml2-java-$VERSION
TAG=$(echo "$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export https://svn.shibboleth.net/java-opensaml2/tags/${TAG}/ $DIR
XZ_OPT=--best tar -c -J -f $TAR \
  --exclude '*.jar' \
  --exclude '*.class' \
$DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
